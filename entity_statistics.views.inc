<?php

/**
 * Implements hook_views_data().
 */
function entity_statistics_views_data() {
  $data['entity_counter']['table']['group'] = t('Entity statistics');

  $entity_type_manager = $entity_type = \Drupal::entityTypeManager();
  $supported_entity_types = \Drupal::config('entity_statistics.settings')->get('entity_types');
  foreach ($supported_entity_types as $entity_type_id) {
    $entity_type = $entity_type_manager->getDefinition($entity_type_id);

    $data['entity_counter']['table']['join'] = [
      $entity_type->getDataTable() => [
        'left_field' => $entity_type->getKey('id'),
        'field' => 'entity_id',
      ],
    ];
  }

  $data['entity_counter']['totalcount'] = [
    'title' => t('Total views'),
    'help' => t('The total number of times the entity has been viewed.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
     ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  $data['entity_counter']['daycount'] = [
    'title' => t('Views today'),
    'help' => t('The total number of times the entity has been viewed today.'),
    'field' => [
      'id' => 'numeric',
      'click sortable' => TRUE,
     ],
    'filter' => [
      'id' => 'numeric',
    ],
    'argument' => [
      'id' => 'numeric',
    ],
    'sort' => [
      'id' => 'standard',
    ],
  ];

  return $data;
}
