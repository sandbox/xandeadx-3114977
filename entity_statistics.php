<?php

use Drupal\Core\DrupalKernel;
use Symfony\Component\HttpFoundation\Request;

chdir('../../..');

$autoloader = require_once 'autoload.php';

$kernel = DrupalKernel::createFromRequest(Request::createFromGlobals(), $autoloader, 'prod');
$kernel->boot();
$container = $kernel->getContainer();

$entity_type = filter_input(INPUT_POST, 'entity_type');
$entity_id = filter_input(INPUT_POST, 'entity_id', FILTER_VALIDATE_INT);
$supported_entity_types = \Drupal::config('entity_statistics.settings')->get('entity_types');

if (in_array($entity_type, $supported_entity_types)) {
  $entity_type_manager = $container->get('entity_type.manager');
  $entity_storate = $entity_type_manager->getStorage($entity_type);

  if ($entity = $entity_storate->load($entity_id)) {
    $container->get('request_stack')->push(Request::createFromGlobals());
    $container->get('entity_statistics.storage')->recordView($entity_id, $entity_type);
  }
}
