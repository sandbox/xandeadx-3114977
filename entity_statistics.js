(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      cache: false,
      url: drupalSettings.entityStatistics.url,
      data: drupalSettings.entityStatistics.data
    });
  });
})(jQuery, Drupal, drupalSettings);
