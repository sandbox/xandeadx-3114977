Installation:

1. Download module in /modules/contrib/entity_statistics and enable his.

2. In root .htaccess replace "RewriteCond %{REQUEST_URI} !/core/modules/statistics/statistics.php$" to
"RewriteCond %{REQUEST_URI} !/modules/contrib/entity_statistics/entity_statistics.php$"

3. Change settings on /admin/config/system/entity_statistics page.
