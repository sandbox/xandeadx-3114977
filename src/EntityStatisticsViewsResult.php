<?php

namespace Drupal\entity_statistics;

/**
 * Value object for passing statistic results.
 */
class EntityStatisticsViewsResult {

  /**
   * @var int
   */
  protected $totalCount;

  /**
   * @var int
   */
  protected $dayCount;

  public function __construct($total_count, $day_count) {
    $this->totalCount = (int) $total_count;
    $this->dayCount = (int) $day_count;
  }

  /**
   * Total number of times the entity has been viewed.
   *
   * @return int
   */
  public function getTotalCount() {
    return $this->totalCount;
  }

  /**
   * Total number of times the entity has been viewed "today".
   *
   * @return int
   */
  public function getDayCount() {
    return $this->dayCount;
  }

}
