<?php

namespace Drupal\entity_statistics;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure statistics settings for this site.
 *
 * @internal
 */
class EntityStatisticsSettingsForm extends ConfigFormBase {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'statistics_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_statistics.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_statistics.settings');

    $entity_types_options = [];
    /** @var ContentEntityTypeInterface[] $content_entity_types */
    $content_entity_types = array_filter(\Drupal::entityTypeManager()->getDefinitions(), function (EntityTypeInterface $entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface;
    });
    foreach ($content_entity_types as $entity_type_id => $entity_type) {
      $entity_types_options[$entity_type_id] = $entity_type->getLabel();
    }

    $form['entity_types'] = [
      '#type' => 'checkboxes',
      '#title' => t('Entity types'),
      '#options' => $entity_types_options,
      '#default_value' => $config->get('entity_types'),
    ];

    $form['ignore_roles'] = [
      '#type' => 'checkboxes',
      '#title' => t('To ignore roles'),
      '#options' => array_map('\Drupal\Component\Utility\Html::escape', user_role_names()),
      '#default_value' => $config->get('ignore_roles'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $selected_entity_types = array_keys(array_diff($form_state->getValue('entity_types'), [0]));
    $selected_ignore_roles = array_keys(array_diff($form_state->getValue('ignore_roles'), [0]));

    $this->config('entity_statistics.settings')
      ->set('entity_types', $selected_entity_types)
      ->set('ignore_roles', $selected_ignore_roles)
      ->save();

    // For reset entity_statistics_views_data() cache
    drupal_flush_all_caches();

    parent::submitForm($form, $form_state);
  }

}
