<?php

namespace Drupal\entity_statistics;

use Drupal\Core\Database\Connection;
use Drupal\Core\State\StateInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides the default database storage backend for statistics.
 */
class EntityStatisticsDatabaseStorage implements EntityStatisticsStorageInterface {

  /**
  * The database connection used.
  *
  * @var \Drupal\Core\Database\Connection
  */
  protected $connection;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $connection, StateInterface $state, RequestStack $request_stack) {
    $this->connection = $connection;
    $this->state = $state;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function recordView($entity_id, $entity_type) {
    return (bool) $this->connection
      ->merge('entity_counter')
      ->keys(['entity_id' => $entity_id, 'entity_type' => $entity_type])
      ->fields([
        'daycount' => 1,
        'totalcount' => 1,
      ])
      ->expression('daycount', 'daycount + 1')
      ->expression('totalcount', 'totalcount + 1')
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function fetchViews($entity_ids, $entity_type) {
    $views = $this->connection
      ->select('entity_counter', 'ec')
      ->fields('ec', ['totalcount', 'daycount'])
      ->condition('entity_id', $entity_ids, 'IN')
      ->condition('entity_type', $entity_type)
      ->execute()
      ->fetchAll();

    foreach ($views as $id => $view) {
      $views[$id] = new EntityStatisticsViewsResult($view->totalcount, $view->daycount);
    }

    return $views;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchView($entity_id, $entity_type) {
    $views = $this->fetchViews([$entity_id], $entity_type);
    return reset($views);
  }

  /**
   * {@inheritdoc}
   */
  public function fetchAll($order = 'totalcount', $limit = 5) {
    assert(in_array($order, ['totalcount', 'daycount']), "Invalid order argument.");

    return $this->connection
      ->select('entity_counter', 'ec')
      ->fields('ec', ['entity_id'])
      ->orderBy($order, 'DESC')
      ->range(0, $limit)
      ->execute()
      ->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function deleteViews($entity_id, $entity_type) {
    return (bool) $this->connection
      ->delete('entity_counter')
      ->condition('entity_id', $entity_id)
      ->condition('entity_type', $entity_type)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function resetDayCount() {
    $statistics_timestamp = $this->state->get('entity_statistics.day_timestamp') ?: 0;
    if (($this->getRequestTime() - $statistics_timestamp) >= 86400) {
      $this->state->set('entity_statistics.day_timestamp', $this->getRequestTime());
      $this->connection->update('entity_counter')
        ->fields(['daycount' => 0])
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function maxTotalCount() {
    $query = $this->connection->select('entity_counter', 'ec');
    $query->addExpression('MAX(totalcount)');
    return (int) $query->execute()->fetchField();
  }

  /**
   * Get current request time.
   *
   * @return int
   *   Unix timestamp for current server request time.
   */
  protected function getRequestTime() {
    return $this->requestStack->getCurrentRequest()->server->get('REQUEST_TIME');
  }

}
